module Main (main) where

import System.Environment (getArgs) 
import Data.List          (intersperse)

main :: IO ()
main = do
    args <- getArgs
    putStrLn $ concat $ intersperse " " args 
