module Main where

import qualified Data.Time.Clock as Clock

-- poor mans timing
-- use profiling and -sstdeer when compiling with +rtsopts

main :: IO ()
main = do
  t1 <- Clock.getCurrentTime
  putStr $ "The sum of number 1 to " ++ show n ++ " is : "
  print $ sum [1..n]
  t2 <- Clock.getCurrentTime
  print $ Clock.diffUTCTime t2 t1
  where n = 1000000 :: Integer
