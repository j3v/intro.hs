% A Whirlwind Tour of Haskell
% [Evgenij Belikov](http://www.macs.hw.ac.uk/~eb96) (Heriot-Watt University, eb120@hw.ac.uk)
% NCR Edinburgh, 15.08.2015


# Overview

<!---
 What is Your Programming Background? (Year of experience -> P. Norvig, Languages)
-->

* Functional Programming

* A Brief History of Haskell

* The Haskell Ecosystem

* Language Features

* Haskell and Mathematics

* Conclusions & Resources

<div align=right>
  <img src='pics/hs.png'/>
</div>


# Functional Programming

* Programming **paradigm** based on Alonzo Church's _λ-calculus_ 

  Computation as composition and application of functions

* Amenable to formal reasoning and semantics-preserving transformations

* Purity (referential transparency), isolated side-effects

  Oriented on _values_ rather than on _mutation_ of variables

* Recursion instead of iteration

* Declarative: focus on the _what_ not the _how_ (high level of abstraction)

* Suitable for exploiting parallelism ([_Church-Rosser_](https://en.wikipedia.org/wiki/Church%E2%80%93Rosser_theorem) theorem)


<!--- 
 ? x=x+1  which value has x? has state; in FP x is immutable

<div align=right>
  <img src='pics/functional.png'/>
</div>

 Church-Rosser: 'applying reduction rules to terms in the lambda calculus, the ordering in which the reductions 
 are chosen does not make a difference to the eventual result'

 - lambda expression in normal form is unique (except for changes in bound variables)
 - different reduction strategies lead to the same normal form (final value, no reduction posible once got normal form expr)
 - normal form reduction will produce a normal form lambda expression if it exists

 Church, Rosser, "Some properties of conversion" (1936)

 Church-thesis: 
 - effectively computable functions are those definable in pure lambda calculus
 - lambda calculus is equivalent to Turing Machines
 - not possible to determine whether a reduction will terminate 
-->


# Functional Programming

* Popularised by John Backus in his Turing Award Lecture (1977)

  Paper: _"Can Programming be Liberated from von Neumann Style?"_ (1978)

* Eloquently argued for by John Hughes (1989)

  Paper: _"Why Functional Programming Matters"_

* Popular functional languages: ML, Scala, Clojure, OCaml, Haskell, F#, Scheme, Racket, Swift

~~~~ {.haskell}
-- Quick sort: sort a list of values in ascending order

qsort [] = []
qsort (x:xs) = qsort [y | y <- xs, y < x] ++ [x] ++ qsort [y | y <- xs, y >= x]

-- (++) is list concatenation function; vs ca. 20 lines in C
~~~~

* Killer app: translation/compilation (easy to add new operations on things)

<!---
  qsort :: Ord a => [a] -> [a]
   ...
  where
    lesser         = qsort [y | y <- xs, y  < x] 
    greaterOrEqual = qsort [y | y <- xs, y >= x]

  * Landin's ISWIM, 700 next languages, Backus' FP

  * Curry-Howard:   types correspond to propositions, (programs) values correspond to proofs
  * Hindley-Milner: type system
-->


# λ-calculus as a Model of Computation 

* can be considered _the smallest universal programming language_
 
     describes the set of computable functions (equivalent to Turing Machines)

* formally defined, see e.g. Barendregt (1984/2013)
  
  Paper/Book: _"Lambda Calculus with Types"_

* variable, e.g. x (can be free or bound to a value)

* functions are formed using _λ-abstraction_, e.g. λx.x*x 

     where '.' separates parameters from the function body

* function application f x (substitution, reduction) (x=2 -> 2*2 -> 4)

* _environment_ maps variables to values

<!--- 
  Haskell implements System FC (System F is second order lambda calculus of lambda2 of the lambda cube), 
  uses Core as core language (IR for optimisations), 
  STG as the Graph Reducer, 
  Cmm as portable Assembler (IR for code generation) 
-->


# Why Haskell?

>"The limits of my language mean the limits of my world." 
Ludwig Wittgenstein

* High level of abstraction and **expressiveness** for productivity

* Safety due to **static strong typing**, **type classes** and **purity**

* Performance (good support for parallelism and concurrency)

* **Lazy** evaluation improves modularity, avoids unnecessary computations

* Clean syntax, concise and elegant code

* Can be interpreted or compiled (folklore: if it compiles, it runs)

* A fun language with a great and supportive community

<!---

  occupies a unique place in language design space
  playful language -- leads to new discoveries
  "Functional Programming with Bananas, Lenses, Envelopes and Barbed Wire"
  within top 100 programming languages (TIOBE Index)
  language shootout game: often only 2x slower than C

"Language shapes the way we think, and determines what we can think about."
Benjamin Lee Whorf, 1897-1941

 strong for parsing and compilation, EDSLs

 purity->referential transparency: (Stoy,1977): 
 'The only thing that matters about an expression is its value,
  and any subexpression can be replaced by any other equal in value.
  Moreover, the value of an expression is, when certain limits, the 
  same wherever it occurs.' -> enables equational reasoning
  e.g. f(x) + f(x) = 2*f(x); also: scope matters;
  but: freedom from execution order; no global state (isolated side-effects)
  alas: IO and in-place updates are harder (-> Monad)

-->


# A Committee Language (IFIP WG 2.8 meeting 1992)

<div align=center>
  <img src="pics/ifip-1992.jpg" align=center width='70%'/>
</div>


# A Brief History of Haskell

* Research language, created in the late 80s, recent standard _Haskell 2010_

* Named after logician Haskell B. Curry

* Uses Graph Reduction as execution model (as opposed to stack/register machine)

* Influenced by David Turner's _Miranda_ and Peter Landin's _ISWIM_ among other languages

* Many modern languages influenced by Haskell: e.g. C++, C#, F#, Java, Python, Rust, Scala

* Many commercial users: 

  Facebook, Well-Typed, Quiviq, FP Complete, Galois, Standard Chartered, Barclays, Jane Street etc.

Paper for more details:
  P. Hudak, J. Hughes, S. Peyton Jones, P. Wadler - _"A History of Haskell:  being lazy with class"_, 
  The Third ACM SIGPLAN History of Programming Languages Conference, 2007


# The Haskell Ecosystem

* [haskell.org](http://www.haskell.org) + wiki

* _haskell-cafe_ and _haskell-beginners_ mailing lists 

* haskell IRC channel and StackOverflow

* GHC compiler suite and GHCi interpreter (among others)

* [Haskell Platform](https://www.haskell.org/platform/)

* _Cabal_ package manager and _hackage_ repository (more recently also stack/stackage)

* [_Hoogle_](https://www.haskell.org/hoogle/), [_Hayoo_](http://hayoo.fh-wedel.de/) (search for functions)
 

# Key Language Features

* Higher-Order Functions (HOFs)

* Algebraic Data Types and Records

* Lazyness (demand-driven evaluation, avoids work duplication)

* Type Classes

* Polymorphism

* Type Inference

* Pattern Matching

* List Comprehensions

* Implicit Memory Management

* Strong Support for Concurrency and Parallelism


# GHC and GHCi

* GHC: industrial-strength optimising compiler

* GHCi: interpreter, convenient to test functions and small code fragments

~~~~ {.bash}

$ ghc -O2 --make -o prog prog.hs     -- compile (add -rtsops for +RTS support)

$ ./prog <opts> [+RTS <rtsopts>]     -- run NB: check GHC wiki for a list of compiler and rts flags

$ ghci                               -- start interpreter/REPL

ghci> :l <fname.hs>                  -- load a file; :r to reload

ghci> :t <expr>                      -- show type of an expression; also :info

ghci> :sprint <expr>                 -- non-invasively check the degree of evaluation

ghci> :m +Data.List                  -- load the _Data.List_ module; use - to unload a module

ghci> :browse Data.List              -- list functions provided by a module

ghci> :q                             -- quit

~~~~

<!---
 => the following in ghci  (maybe also show how to compile ?)
 :l, :t :m +Data.List
-->


# Basic Built-in Types

* values (usually _boxed_)

~~~~ {.haskell}

True, False             -- Bool  NB: -- comment;  {- multi-line comment -}

1                       -- Int (bounded)

100000000000000000000   -- Integer (unbounded)

3.14                    -- Double (Float also supported)

'd'                     -- Char

"hello"                 -- String, type synonym for [Char]

['h','e','l','l','o']   -- [Char], a list of characters 

undefined               -- of most general type: a

()                      -- "bottom" of type ()

~~~~


# Built-in Lists

* immutable, zero indexed, homogeneous, nestable
 
~~~~ {.haskell}

0 : 1 : []                -- `cons` (:) and `nil`([]) used to construct a list [0,1]

let ls = [1,2,3,4,5]      -- binds ls to a list (shorthand notation: [1..5])

ls !! 0                   -- returns element on the position 0

head ls                   -- 1

tail ls                   -- [2,3,4,5]

[1,2] ++ [3,4]            -- list concatenation: [1,2,3,4]

0:[1,2]                   -- prepending an element: [0,1,2]

length ls                 -- 5 (does not evaluate the elements)

reverse, flatten          -- and more functions in the Data.List module

[['a'],['b'],['c','d']]   -- what is the type of this list?

~~~~


# Built-in Tuples

* immutable, ordered, fixed-size, heterogeneous, nestable

~~~~ {.haskell}

()                        -- 0-ary tuple

(1, 2)                    -- tuple of type (Int, Int)

(3.14, "hello")           -- (Double, String)

fst (3, 4)                -- 3

snd (3, 4)                -- 4

(1, (2.34, "hi"), 'y')    -- what is the type of this tuple?
~~~~


# Functions: Main Building Blocks of Programs

* to define: specify type signature (optional), name, parameters, body expression

* recursion: definition in terms of the function itself (base case + recursive case)

* to use: apply to a value or expression; compose with other functions

* currying: function with many parameters can be represented by a sequence of function applications to one argument (intermediate results being functions)

~~~~ {.haskell}

constOne = 1               -- constOne :: Int (actually Num a => a)

addTwo a b = a + b         -- addTwo :: Int -> Int -> Int (actually Num a => a -> a -> a)

isEmpty [] = True          -- isEmpty :: [a] -> Bool  cf null library function
isEmpty _  = False         -- _ is a wildcard ("don't care") 
                           -- `a` is a type variable
max x y = if x > y then x  -- conditional evaluation alt.: case x > y of
                   else y  --                                True  -> ...
                           --                                False -> ...
sum [] = 0                 -- base case         what is the type of sum?
sum (x:xs) = x + sum xs    -- recursive case      
    
~~~~


# Function Application: An Example

~~~~ {.haskell}
sum :: Num a => [a] -> a   -- more details later
sum [] = 0
sum (x:xs) = x + sum xs

sum [1,2,3]                -- example
-> 1 + sum [2,3]         
-> (1 + (2 + sum [3])      -- can you see any issues with this?
-> (1 + (2 + (3 + sum [])
-> (1 + (2 + (3 + 0)
-> (1 + (2 + 3))
-> 1 + 5
-> 6

sum' acc []     = acc               -- what is the type of sum'?
sum' acc (x:xs) = sum' (acc+x) xs   -- tail recursive

sum' 0 [1,2,3]   -- how does the execution look like ? is there difference to sum?

-- exercise: can you still spot some issues ?

--           write a product function
~~~~

# Local Bindings

* labelling expressions

* **let** ... **in** ... (can be nested)

* **where** (scopes over several guarded expressions)

* layout rule: expressions must be indented to the right of the clauses

~~~~ {.haskell}

let 
  x = 2 
in 
  x * x

y = x * x
  where 
    x = 2

~~~~

* shadowing (variable in the innermost scope is visible and others with the same name are not)


# Pattern Matching and Guards

* done top to bottom (should be exhaustive)

~~~~ {.haskell}

l@(x:xs)           -- captures whole list as l (the list should be non-empty)
                   -- and head as x and tail as xs  (similarly for tuples: (x,y))

take _ [] = []     -- can't take from an empty list
take 0 _  = []     -- what is the type of take ?
take n (x:xs) = x : take (n-1) xs
                   -- define the drop function 
~~~~

* top guard is checked first (guard expression should evaluate to True to match)

~~~~ {.haskell} 

sign n | x >  0 =  1     -- exercise: define the factorial function
       | x == 0 =  0     --               n! = product of 1 to n
       | x <  0 = -1     --          e.g. 3! = 1 * 2 * 3 = 6 

FizzBuzz x
  | x `mod` 3 == 0 && x `mod` 5 == 0  = "FizzBuzz"
  | x `mod` 3 == 0                    = "Fizz"    
  | x `mod` 5 == 0                    = "Buzz"    
  | otherwise      {- catch all -}    = show x 

~~~~


# Higher-Order Functions and Function Composition

* functions are (first-class) values, can be bound to variables 

* can be passed as inputs and returned as outputs

~~~~ {.haskell}
map :: (a -> b) -> [a] -> [b]        -- applies a function to each element
map _ []     = []                    -- of a given list
map f (x:xs) = (f x) : map f xs

map (+2) [1,2,3]                     -- [3,4,5]

sum = foldl' (+) 0                   -- reduction; how is it implemented?
                                     -- challenge: implement map in terms of fold
odds = filter (`div` 2 == 0)         -- implement filter :: (a -> Bool) -> [a] -> [a]
                                     
dotProd xs ys = sum $ zipWith (*) xs ys   -- $ is function application

factorial = product [1..n]           -- exercise: implement reverse and length using fold

[(+),(-),(*),(/)]                    -- list of functions: :: [(Num a => a -> a -> a)]

wordCount = (count . words . lines) input
~~~~
<!--- map = foldr  ((:).f)   [] 
  reverse = foldl' (flip(:)) []
   length = foldl' (\acc x -> 1 + acc) 0
vs
   len xs = go xs 0
      where go   []   acc = acc
            go (x:xs) acc = go acc $! (1+acc)   -- ? needs BangPatterns ?
-->

# Lambdas (Anonymous Functions) and Currying

* often used as inputs to HOFs

* only single clause allowed (need to ensure it covers all inputs)

* all functions in Haskell take one argument (and often return a function)

* partial application: supplying less arguments return another function

~~~~ {.haskell}

sq      = \x -> x * x

addTwo  = \x y -> x + y             -- what is the type of addTwo
let f   = addTwo 2                  -- what is the type of f ?

prodzip = zipWith (\x y -> x * y)

trim_hd = dropWhile isSpace

addTwo 2 4 
-> 6

f 4
-> 6
~~~~


# Parametric Polymorphism

* value is polymorphic if there is more than one type it can have

* some functions can be generalised to work on many data types

* type variables are unconstrained

~~~~ {.haskell}

id :: a -> a                           -- the idenity function; output of the same type as input

length :: [a] -> Int                   -- does not evaluate the elements 

length [1,2,3] == length ['a','b','c'] -- True

fst :: (a, b) -> a

snd :: (a, b) -> b                     -- a and b are variables at type level

map :: (a -> b) -> [a] -> [b]

~~~~


# List Comprehensions

* essentially syntaxtic sugar for filter

* [ f elem1 ... elemN | elem1 <- list1, elemN <- listN, predicates-for-filtering ]

~~~~ {.haskell}

odds    = [ x | x <- [1..], x `div` 2 /= 0]  -- infinite list, don't print

take 5 odds                                  -- [1,3,5,7,9]

squares xs = [ x * x | x <- xs]              -- x * x is the same as (*) x x       

combinations = [ (r,g,b) | r <- l, g <- l, b <- l ] 
  where l = [0..255]

primes = sieve [2..]
  where  -- demonstrates list comprehensions (not efficient)
    sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p /= 0]

-- exercise: how could sieve be improved?
~~~~
<!--- vs 
  where sieve xs  = [x | x <- xs, isPrime x]
        isPrime x = any filter (>0) $ map (x `div`) [2..sqrt]     
-->


# User-Defined Algebraic Data Types

* **data** constructor must be capitalised

* data types can be recursive and paramteric

~~~~ {.haskell}

type String = [Char]                   -- type synonym

type Address = [String]

data Bool   = True 
            | False
 
data List a = Nil
            | Cons a (List a)          -- e.g. Cons is type constructor

data Tree a = Leaf a
            | Node a (Tree a) (Tree a) 

newtype UID = UID a

~~~~

* **newtype** (for efficiency; requires exactly one constructor with one field)

<!--- 
 exercise: depth of the tree, isBalanced, mkBalancedTree (use splitAt)
 more: http://www.macs.hw.ac.uk/~hwloidl/Courses/F21DP/tutorial10.html
 Brent Yorgey, Chris Allen
 Explain: algebraic data type != abstract data type
-->

# Records

* reduce the amount of _boilerplate_ code for accessor functions

~~~~ {.haskell}

data Customer = Customer {
      cID   :: Interger
    , cName :: String
    , cAddr :: Address
} deriving (Show)

-- defines accessor functions such as cID :: Customer -> Integer

-- creating a value of the Customer type
c  = Customer 153 "Ailing Prone" ["Advanced Base Camp, Rankling La, Yogistan"]

-- or using record syntax; can use different order
c' = Customer {
       cID = 728,
     , cAddr = ["Fast Lane, Boulder, CO, USA"]
     , cName = "Warple Wharton" 
     }

cID c'  -- 728
~~~~


# Libraries, IO and the _do_ Syntax

* interface to the world
 
~~~~ {.haskell}

import System.Environment(getArgs)      -- could add hiding(<function_name>)

-- import LibL as L                     -- qualified import, call functions: L.<fname>

module Main (main) where                -- Main module exports main function
  main :: IO ()                         -- finally: hello world example
  main = putStrLn "hello world"         -- see src/hello.hs

echo = do c <- getChar  -- <- binds result of an an action (with side-effects) within a do block
          putchar c     -- let ... = binds result of a pure function

import Data.List(intersperse)            -- import a function that inserts a specified element 
                                         -- between the elementes of a given list
main :: IO ()
main = do                                -- see src/args.hs
    args <- getArgs                      -- get command line arguments and then print them
    putStrLn $ concat $ intersperse " " args
~~~~
<!--- TODO: metion return ? read (args!!0) :: Int -->


# Type Classes

* define interfaces (can be user-defined)

* ad-hoc polymorphism 

~~~~ {.haskell}

class  Eq a  where              -- context Eq specifies that value can be checked for equality
  (==), (/=)  :: a -> a -> Bool
  x /= y      =  not (x == y)

instance Eq Integer where
  x == y = x `integerEq` y

class  (Eq a) => Ord a  where   -- value can be sorted; class extension Ord has Eq functions
  (<), (<=), (>=), (>)  :: a -> a -> Bool
  max, min              :: a -> a -> a

class Num a where           -- support for arithmetic operations
  (+) ...
   
class Show a where          -- support for creation of a String representation of the element
  show :: a -> String

-- others e.g. Foldable, Traversable, Applicative
~~~~


# Infinite and Circular Data Structures

* data structure refers to itself

* relies on lazyness: separating generators and consumers

~~~~ {.haskell}

numsFrom n = [n..]         -- can have a stride; can be increasing or decreasing

-- exercise: define numsFrom using a) map and b) list comprehensions 

take 5 $ numsFrom 10       -- [10,11,12,13,14]

let ones = 1 : ones        -- no evaluation until the values are demanded

take 5 ones                -- [1,1,1,1,1]

fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

-- exercise: define fibs using list comprehensions or scanl
~~~~
<!--- 
  nF  = 1 : map (+1) nF
  nF' = 1 : [ x+1 | x <- nF' ]
  fibs2 = 0 : 1 : [ a+b | a <- fibs2, b <- tail fibs2 ]
  fibs3 = 0 : scanl (+) 1 fibs3
-->

# Relationship to Mathematics

>"Functional programming combines the flexibility and power of abstract mathematics with the intuitive clarity of abstract mathematics." xkcd.com

* Equational reasoning ("Programming with Equations")

* (Generalised) Algebraic Data Types

* Category Theory (Monad, Functor, Applicative, Arrow)

* Higher order function composition

* Combinators

* Curry-Howard Isomorphism (Types as propositions, programs as proofs)


# Conclusions

>"A language that doesn't affect the way you think about programming, is not worth knowing."
Alan Perlis

* FP will make you think differently about programming

* FP will help you become a better programmer

* Higher productivity due to higher level of abstraction and safety

* Better maintainability of programs due to composability

* Many connections to mathematics

* It's fun! Give it a go!

* Join [edLambda](http://www.edlambda.co.uk/): Edinburgh's Mostly Functional Programming Meetup


# Resources

* [tryhaskell](http://tryhaskell.org/)

* [bitemyapp](https://github.com/bitemyapp/learnhaskell) tutorial and [Haskell Cheatsheet](http://cheatsheet.codeslower.com/) 

* haskell.org (+ wiki, many links and explanations)

* Selected Books

    * [Learn You a Haskell for Great Good](http://learnyouahaskell.com/chapters) (M. Lipovaca)

    * Programming in Haskell, (G. Hutton)

    * Haskell: The Craft of Functional Programming (S. Thompson)

    * [Real World Haskell](http://book.realworldhaskell.org/read/) (B. O'Sullivan, D. Stewart, and J. Goerzen)

    * [Haskell School of Music -- From Signals to Symphonies](http://haskell.cs.yale.edu/?post_type=publication&p=112) (P. Hudak)

    * Functional Programming (A. Field and P. Harrison)

    * Haskell 2010 Report

<!---

http://ohaskell.dshevchenko.biz/en/chapters.html  -- 'humanly?'

# Testing with QuickCheck and tasty   -> see paper and pearl

λ> import Test.QuickCheck
λ> import Data.Foldable
λ> import Data.Sequence
λ> quickCheck (\i -> [i :: Int] ==
                       toList (empty |> i))

prop_rev :: [Int] -> Bool
prop_rev xs = reverse (reverse xs) == xs
quickCheck prop_rev


scripting with runhaskell

main :: IO ()
main = do
  input <- getContents
  let wordCount = length (words input)
  print wordCount

vs

main = getContents >>= print . length . words 
(.) ($)

referential transparency
This means that function invocation can be freely replaced with its return value without changing semantics

lazy:

    Call by need (outside in), not call by value (inside out)
    Non-strict evaluation separates equation from execution
    No need for special forms for control flow, no value restriction
    Enables infinite or cyclic data structures
    Can skip unused computation (better minimum bounds)
http://bob.ippoli.to/haskell-for-erlangers-2014/#/lazy

fib = 0 : 1 : zipWith (+) fib (tail fib)


Abstractions
Monoid  -- Has an identity and an associative operation 
Functor -- Anything that can be mapped over (preserving structure) 
Applicative -- Functor, but can apply function from inside 
Monad   -- Applicative, but can return any structure 

A monad is just a monoid in the category of endofunctors, what's the problem?

return probably doesn't mean what you think it means.


sum :: Num a => [a] -> a
sum []     = 0
sum (x:xs) = x + sum xs

sum :: Num [a] => [a] -> a
sum = go 0
  where
    go acc (x:xs) = go (acc + x) (go xs)
    go acc []     = acc

{-# LANGUAGE BangPatterns #-}

sum :: Num [a] => [a] -> a
sum = go 0
  where
    go !acc (x:xs) = go (acc + x) (go xs)
    go  acc []     = acc

multi-line comments are nestable


# Dependently-Typed Programming

TODO

# Foreign Function Interface

* Calling Hell form Heaven and Heaven from Hell

* e.g. call C from Haskell

TODO


# Optimisation

* profiling

* space leaks

* unnecessary traversals and intermediate data structures

~~~~ {.haskell}
mean :: [Double] -> Double
mean xs = s / n
  where (s, n) = foldl' (\ (s, n) x -> (s+x, n+1)) (0, 0) xs
~~~~

Doesn't allocate on each iteration:

~~~~ {.haskell}
data StrictPair a b = SP !a !b

mean2 :: [Double] -> Double
mean2 xs = s / n
  where SP s n = foldl' (\ (SP s n) x -> SP (s+x) (n+1)) (SP 0 0) xs
~~~~

# Parallelism

TODO

Eval Monad

Par Monad


# Concurrency

forkIO

MVar

TODO


# Transactional Memory

TVar

STM Monad

TODO


# Distributed Programming

* Cloud Haskell

* HdpH

* GUM

TODO


no semantics (only for parts), based on System FC (Girard: System F)


EXERCISES:

LYAH: generate right triangles with Int-long sides, length <= 10, sum of sides 24
      [(a,b,c) | a <- xs, b <-xs, c <- xs, a+b+c == 24] where xs = [1..10]

how many combinations of baskin robbins icecreams are there for three flavours out of 24?

Write a function that removes elements from a list:



-- It is a special case of 'nubBy', which allows the programmer to supply
-- their own equality test. (from Data.List)
nub                     :: (Eq a) => [a] -> [a]
#ifdef USE_REPORT_PRELUDE
nub                     =  nubBy (==)
#else
-- stolen from HBC
nub l                   = nub' l []             -- '
  where
    nub' [] _           = []                    -- '
    nub' (x:xs) ls                              -- '
        | x `elem` ls   = nub' xs ls            -- '
        | otherwise     = x : nub' xs (x:ls)    -- '
#endif

removeDuplicates3 = foldr (\x seen -> if x `elem` seen then seen else x : seen) [] 

rmdups :: (Ord a) => [a] -> [a]
rmdups = map head . group . sort

import qualified Data.Set as Set

rmdups :: Ord a => [a] -> [a]
rmdups = rmdups' Set.empty where
  rmdups' _ [] = []
  rmdups' a (b : c) = if Set.member b a
    then rmdups' a c
    else b : rmdups' (Set.insert b a) c



# Benchmarking using Criterion

import Data.List
import qualified Data.Set as Set
import Criterion.Main 
import System.Random
import Control.Applicative

main :: IO ()
main = defaultMain [
    bgroup "input1" [
      bench "nub" $ nfIO $ nub <$> generateInput (0, 100) 1000,
      bench "poorsod1" $ nfIO $ poorsod1 <$> generateInput (0, 100) 1000,
      bench "poorsod2" $ nfIO $ poorsod2 <$> generateInput (0, 100) 1000,
      bench "volkov" $ nfIO $ volkov <$> generateInput (0, 100) 1000,
      bench "scvalex" $ nfIO $ scvalex <$> generateInput (0, 100) 1000
    ],
    bgroup "input2" [
      bench "nub" $ nfIO $ nub <$> generateInput (0, 1000) 1000,
      bench "poorsod1" $ nfIO $ poorsod1 <$> generateInput (0, 1000) 1000,
      bench "poorsod2" $ nfIO $ poorsod2 <$> generateInput (0, 1000) 1000,
      bench "volkov" $ nfIO $ volkov <$> generateInput (0, 1000) 1000,
      bench "scvalex" $ nfIO $ scvalex <$> generateInput (0, 1000) 1000
    ]
  ]

generateInput :: (Int, Int) -> Int -> IO [Int]
generateInput range amount = 
  sequence $ replicate amount $ randomRIO range

poorsod1 :: Eq a => [a] -> [a]
poorsod1 = rdHelper []
    where rdHelper seen [] = seen
          rdHelper seen (x:xs)
              | x `elem` seen = rdHelper seen xs
              | otherwise = rdHelper (seen ++ [x]) xs

poorsod2 :: Eq a => [a] -> [a]
poorsod2 = foldl (\seen x -> if x `elem` seen
                              then seen
                              else seen ++ [x]) []

volkov :: Ord a => [a] -> [a]
volkov = rmdups' Set.empty where
  rmdups' _ [] = []
  rmdups' a (b : c) = if Set.member b a
    then rmdups' a c
    else b : rmdups' (Set.insert b a) c

scvalex :: (Ord a) => [a] -> [a]
scvalex = map head . group . sort

-->
